An example cohort analysis project using data from the [UC Irvine machine learning repository](https://archive.ics.uci.edu/ml/datasets/online+retail).

The dataset provided can be loaded directly into postgres.

```
gunzip < sql/create_orders.sql.gz | psql
```

To create the cohorts table from the orders.

```
psql -f sql/create_cohorts.sql
```
