select
  cohort_week,
  sum(revenues) / count(distinct(customer_id)) as arpu_60_day
from cohorts
-- Only include revenue from weeks within ~60 days
where invoice_week - cohort_week <= 60
group by 1
