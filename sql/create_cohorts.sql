create table cohorts as (
  select
    customer_id,
    invoice_week,
    sum(amount) as revenues,
    -- The first week the customer placed an order is it's cohort
    min(invoice_week) over (partition by customer_id) as cohort_week
  from orders
  group by 1, 2
  order by 1, 2
);
