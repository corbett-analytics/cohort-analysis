select
  invoice_week,
  sum(revenues) as revenues
from cohorts
group by 1
order by 1 asc
